import WXHttpRequest from './WXHttpRequest';

/**
 * 根据当前运行环境生成相应的发送Http请求对象
 */
export default class HttpRequest {
    /**
     * 根据当前运行环境生成相应的发送Http请求对象HttpRequest类构造函数
     * @param {String} contentType 发送形式，默认form表单（json、form）
     */
    constructor(contentType) {
        this.contentType = contentType;
        this.request = null;
        this.cancelToken = '';

        this.request = new WXHttpRequest();
        this.cancelFun = () => {
            this.cancelToken = new Date().getTime();
            this.request.cancelToken = this.cancelToken;
            this.request.abort();
        };
    }

    /**
     * 判断是否是手动取消导致请求出错
     * @param {Object} error 取消出错对象
     * @return {Boolean} 返回是否是手动取消导致请求出错
     */
    static isCancel(error) {
        return error.cancelToken === this.cancelToken;
    }
}
