import { clone } from '@zibu/common-base';
import { dataFormat } from '@zibu/common-base';
import AjaxService from './AjaxService';

export { AjaxService, clone, dataFormat };
